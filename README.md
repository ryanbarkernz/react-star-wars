# Star Wars Character Ratings

Setup:
* npm install
* npm i -g json-server
* json-server -p 8080 --watch db.json
* npm start

To do:
* Tidy up a lot...
* Add comments
* Revisit variable/action/function names 
* Revisit Componentisation
* Relook at API calls and data structures
* Create fallbacks and catch errors (eg if json server isn't running, show at least some data)
* Proptypes
* Show likes vs. dislikes as opposed to a single counter
* Limit ratings by cookies / GEO
* Edit / delete comments
* Better CSS structure