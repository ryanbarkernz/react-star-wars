import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { loadPeople } from '../actions'
import Home from './Home'
import Detail from './Detail'

class App extends Component {

  componentWillMount() {
    this.props.loadPeople()
  }

  render() {
    return (
      <div>
        <main>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/:person" component={Detail} />
          </Switch>
        </main>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  data: state.data,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  loadPeople
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
