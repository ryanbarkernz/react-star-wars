import React, {Component} from 'react'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Loading from '../components/Loading'
import Comment from '../components/Comment'
import {
  loadPeople,
  updatePerson,
  createPersonData
} from '../actions'

class Detail extends Component {

  constructor(props) {
    super(props)
    
    this.state = {
      comment: '',
      user: ''
    }

    this.updateComment = this.updateComment.bind(this)
    this.updateUser = this.updateUser.bind(this)
    this.submitComment = this.submitComment.bind(this)
  }

  updateComment(e) {
    this.setState({
      comment: e.target.value
    })
  }

  updateUser(e) {
    this.setState({
      user: e.target.value
    })
  }

  submitComment(e,a) {
    e.preventDefault()
    const person = this.props.people.results.filter(person => person.slug === this.props.location.pathname.replace('/', ''))[0]

    person.person_data.comments.push({
      user: this.state.user,
      text: this.state.comment
    })

    this.props.updatePerson({...person})

    this.setState({
      comment: '',
      user: ''
    })
  }

  render() {
    const props = this.props
    if (!props.people.results) return <Loading />

    const person = props.people.results.filter(person => person.slug === props.location.pathname.replace('/', ''))[0]
    if (!person || !person.person_data) return <Loading />

    return (
      <div>
        <h1>{person.name}</h1>
        <h2>Character information:</h2>
        <ul>
          <li><strong>Birth Year:</strong> {person.birth_year}</li>
          <li><strong>Eye Colour:</strong> {person.eye_color}</li>
          <li><strong>Gender:</strong> {person.gender}</li>
          <li><strong>Hair Colour:</strong> {person.hair_color}</li>
          <li><strong>Height:</strong> {person.height}</li>
          <li><strong>Mass:</strong> {person.mass}</li>
          <li><strong>Skin Colour:</strong> {person.skin_color}</li>
        </ul>

        <h2>Comments:</h2>

        {person.person_data.comments.map(comment => {
            if (!comment.text) return <div className="no-comments">No comments yet! Add one below.</div>
            return (
              <div className="comment" key={Math.floor((Math.random() * 1000000) + 1)}>
                <blockquote>{comment.text}</blockquote>
                <small>{comment.user}</small>
              </div>
            )
          })
        }

        <Comment comment={this.state.comment} name={this.state.user} handleTextareaChange={this.updateComment} handleNameChange={this.updateUser} handleSubmit={this.submitComment}/>

        <div className="back-link">
          <Link to="/">Back</Link>
        </div>

      </div>
    )
  }
}

const mapStateToProps = state => ({
  people: state.data.people
})

const mapDispatchToProps = dispatch => bindActionCreators({
  loadPeople,
  updatePerson,
  createPersonData
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Detail)
