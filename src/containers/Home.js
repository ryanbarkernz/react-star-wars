import React, {Component} from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { filterText } from '../actions'
import Loading from '../components/Loading'
import Filter from '../components/Filter'
import List from '../components/List'

class Home extends Component {

  constructor(props) {
    super(props)
    this.updateFilterText = this.updateFilterText.bind(this)
  }

  updateFilterText(e) {
    this.props.filterText(e.target.value.toLowerCase())
  }

  render() {
    if (!this.props.people) return <Loading />

    return (
      <div>
        <h1>Star Wars Character Ratings</h1>
        <Filter val={this.props.filter} handleChange={this.updateFilterText} />
        <List filter={this.props.filter} people={this.props.people}/>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  people: state.data.people.results,
  filter: state.data.filter
})

const mapDispatchToProps = dispatch => bindActionCreators({
  filterText,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
