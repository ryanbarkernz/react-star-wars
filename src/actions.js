const swapiUrl = 'https://swapi.co/api/people/'
const siteDataUrl = 'http://localhost:8080/people'

export const filterText = (text) => {
  return dispatch => {
    dispatch({
      type: 'FILTER',
      filter: text
    })
  }
}

export const updatePerson = (person) => {
  return dispatch => {
    const newState = {
      "id": person.person_data.slug,
      "rating": person.person_data.rating,
      "comments": person.person_data.comments
    }

    fetch(`${siteDataUrl}/${person.slug}`, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newState)
    })
    .then(res => res.json())
    .then(person => {
      dispatch({
        type: 'UPDATE_PERSON',
        person: person
      })
    })
  }
}

export const loadPeople = () => {
  return dispatch => {
    fetch(swapiUrl)
      .then(res => res.json())
      .then(people => {

        // add a slug to each person
        people.results.map( (person) => {
          person.slug = person.name.replace(/\s+/g, '-').toLowerCase()
          return {...person}
        })

        dispatch({
          type: 'GET_PEOPLE',
          people: people
        })

        // add comments and ratings
        fetch(siteDataUrl)
        .then(res => res.json())
        .then(peopleSavedData => {

          let index = -1
          people.results.map( (person) => {
            index = peopleSavedData.findIndex(personData => {
              return person.slug === personData.id
            })

            if (index !== -1) {
              dispatch({
                type: 'GET_PERSON_DATA',
                slug: person.slug,
                person_data: peopleSavedData[index]
              })
            } else {
              const initialState = {
                "id": person.slug,
                "rating": 0,
                "comments": []
              }

              fetch(siteDataUrl, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify(initialState)
              })
              .then(res => res.json())
              .then(person => {
                dispatch({
                  type: 'CREATE_PERSON_DATA',
                  person_data: initialState
                })
              })
            }
            return false
          })

        })

        // add homeworld data
        people.results.map( (person) => {
          fetch(person.homeworld)
          .then(res => res.json())
          .then(homeworld_data => {
            dispatch({
              type: 'GET_PERSON_DETAIL',
              slug: person.slug,
              homeworld_data: homeworld_data
            })
          })
          return false
        })

      })

  }
}
