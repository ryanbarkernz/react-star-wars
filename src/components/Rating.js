import React, {Component} from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { savePerson, updatePerson } from '../actions'

class Rating extends Component {

  constructor(props) {
    super(props)
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    let value = e.currentTarget.value === 'like' ? 1 : -1
    e.currentTarget.focus()
    this.props.person.person_data.rating = this.props.person.person_data.rating + value
    this.props.updatePerson({...this.props.person})
  }

  render() {
    return (
      <div>
        <h3>{this.props.person.person_data ? this.props.person.person_data.rating : 0}</h3>
        <button value="like" onClick={this.handleClick}>Like</button>
        <button value="dislike" onClick={this.handleClick}>Dislike</button>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({})

const mapDispatchToProps = dispatch => bindActionCreators({
  savePerson,
  updatePerson
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Rating)
