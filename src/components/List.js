import React from 'react'
import { Link } from 'react-router-dom'
import '../styles/components/list.css'
import Rating from './Rating'

const List = (props) => {

  props.people.sort((a, b) => {
    let newA = a.person_data ? a.person_data.rating : 0
    let newB = b.person_data ? b.person_data.rating : 0

    return newB - newA  ||  a.name.localeCompare(b.name)
  })
  

  return (
    <ul className="cards">
      {props.people.map((person) => {
        // If not yet loaded or filtered do not display
        if (!person.homeworld_data || person.name.toLowerCase().search(props.filter) === -1) return false
        return (
          <li key={Math.floor((Math.random() * 1000000) + 1)} className="card">
            <Link to={person.slug}>
              {person.name}
            </Link>
            <small>{person.homeworld_data.name}</small>
            <Rating person={person} />
          </li>
        )
      })}
    </ul>
  )
}

export default List
