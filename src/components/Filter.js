import React from 'react'
import '../styles/components/filter.css'

const Filter = (props) => {
  return (
    <div className="filter-form">
      <form>
        <label htmlFor="filter">Filter by name:</label>
        <input type="text" id="filter" placeholder="Search" value={props.val} onChange={props.handleChange}/>
      </form>
    </div>
  )
}

export default Filter
