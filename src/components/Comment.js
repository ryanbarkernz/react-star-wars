import React from 'react'
import '../styles/components/comment.css'

const Comment = (props) => {
  return (
    <div className="comment-form">
      <form onSubmit={props.handleSubmit}>
        <label htmlFor="name">Your name:</label>
        <input type="text" id="name" required placeholder="John Doe..." value={props.name} onChange={props.handleNameChange}/>
        <label htmlFor="comment">Add a comment:</label>
        <textarea id="comment" required placeholder="Comment..." value={props.comment} onChange={props.handleTextareaChange}/>
        <button type="submit">Submit</button>
      </form>
    </div>
  )
}

export default Comment
