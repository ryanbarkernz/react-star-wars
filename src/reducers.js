import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

function data(state = {
  people: {},
  filter: ''
}, action) {

  let index = 0

  switch (action.type) {
    case 'GET_PEOPLE':
      return {
        ...state,
        people: action.people
      }

    case 'GET_PERSON_DATA':
      index = state.people.results.findIndex(person => person.slug === action.slug)
      state.people.results[index].person_data = action.person_data 
      return {...state}

    case 'GET_PERSON_DETAIL':
      index = state.people.results.findIndex(person => person.slug === action.slug)
      state.people.results[index].homeworld_data = action.homeworld_data 
      return {...state}

    case 'UPDATE_PERSON':
      index = state.people.results.findIndex(person => person.slug === action.person.slug)
      if (index !== -1) state.people.results[index] = action.person
      return {...state}

    case 'CREATE_PERSON_DATA':
      index = state.people.results.findIndex(person => person.slug === action.person_data.id)
      if (index !== -1) state.people.results[index].person_data = action.person_data
      return {...state}

    case 'FILTER':
      return {
        ...state,
        filter: action.filter
      }

    default:
      return state
  }
}

export default combineReducers({
  router: routerReducer,
  data
})
